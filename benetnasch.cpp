#include <iostream>
#include <iomanip>
#include <algorithm>
#include <chrono>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "bmath.cpp"
#include "btime.cpp"
#include "sdlhelpers.cpp"

typedef unsigned long i_entityid;

namespace World
{
    namespace Sys
    {
        SDL_Window * MainWindow;
        SDL_Renderer * Renderer;
        SDL_Rect shape = {0, 0, 800, 600};
    }
    template<typename CType>
    CType* Add(CType* NewComponent);
    template<typename CType>
    bool Refers(i_entityid myEntity);
    template<typename CType, typename CTypeFrom>
    CType * From (CTypeFrom Component);
    template<typename CType>
    CType * Of (i_entityid myEntity);
    template<typename CType, typename CTypeFrom>
    CType * Of (CTypeFrom * Component);
    template<typename CType>
    std::vector<CType*> Every();
    
    struct Component
    {
        Component(i_entityid myEntity);
        virtual ~Component();
        
        i_entityid entityID;
        
        template<typename CType>
        void MakeDependency();
    };

    /* Components */
    Component::Component(i_entityid myEntity) : entityID(myEntity)
    {
        Add(this);
    }
    Component::~Component()
    { }
    
    template<typename CType>
    void Component::MakeDependency()
    {
        if(!Refers<CType>(entityID))
        {
            new CType (entityID);
        }
    }
    
    /* Component forward declarations */
    
    struct Gravitational : public Component
    {
        Gravitational(i_entityid myEntity);
        double gravity;
    };
    struct BoxDrawable : public Component
    {
        BoxDrawable(i_entityid myEntity);
        
        SDL_Rect* getShape();
        
        private:
            SDL_Rect shape;
    };
    
    
    struct Position : public Component
    {
        Position(i_entityid myEntity);
        double x, y;
    };
    Position::Position(i_entityid myEntity) : Component(myEntity), x(0), y(0)
    { }
    
    
    struct Motion : public Component
    {
        Motion(i_entityid myEntity);
        
        double direction();
        double speed();
        double hspeed, vspeed;
        
        void addPolar( double magnitude, double direction );
        void setPolar( double magnitude, double direction );
    };
    Motion::Motion(i_entityid myEntity) : Component(myEntity), hspeed(0), vspeed(0)
    {
    }
    
    double Motion::direction()
    {
        return rad2deg(atan2( vspeed, hspeed ));
    }
    double Motion::speed()
    {
        return sqrt( pow( vspeed, 2 ) + pow( hspeed, 2 ) );
    }
    void Motion::addPolar( double magnitude, double direction )
    {
        hspeed += cos(deg2rad(direction)) * magnitude;
        vspeed += sin(deg2rad(direction)) * magnitude;
    }
    void Motion::setPolar( double magnitude, double direction )
    {
        hspeed = cos(deg2rad(direction)) * magnitude;
        vspeed = sin(deg2rad(direction)) * magnitude;
    }
    
    
    struct Hull : public Component
    {
        Hull(i_entityid myEntity);
        double height, width, xoffset, yoffset;
    };
    Hull::Hull(i_entityid myEntity) : Component(myEntity), height(0), width(0), xoffset(0), yoffset(0)
    { }
    
    
    struct HullCollider : public Component
    {
        HullCollider(i_entityid myEntity);
        bool meeting(HullCollider * collider);
        bool place_meeting(double x, double y, HullCollider * collider);
        bool place_meeting_filter(double x, double y, std::function<bool(HullCollider*)> lambda);
        double contact_filter(std::function<bool(HullCollider*)> lambda);
        double move_contact_filter(double hvec, double vvec, std::function<bool(HullCollider*)> lambda);
        double move_outside_filter(double hvec, double vvec, std::function<bool(HullCollider*)> lambda);
    };
    HullCollider::HullCollider(i_entityid myEntity) : Component(myEntity)
    {
        MakeDependency<Hull>();
        MakeDependency<Position>();
    }
    bool HullCollider::meeting(HullCollider * collider)
    {
        auto pos = Of<Position>(entityID);
        return place_meeting(pos->x, pos->y, collider);
    }
    bool HullCollider::place_meeting(double x, double y, HullCollider * collider)
    {
        auto hull = Of<Hull>(entityID);
        auto ohull = Of<Hull>(collider);
        auto opos = Of<Position>(collider);
        if(x + hull->xoffset < opos->x + ohull->width + ohull->xoffset &&
           y + hull->yoffset < opos->y + ohull->height + ohull->yoffset &&
           opos->x + ohull->xoffset < x + hull->width + hull->xoffset &&
           opos->y + ohull->yoffset < y + hull->height + hull->yoffset)
            return true;
        else
            return false;
    }
    bool HullCollider::place_meeting_filter(double x, double y, std::function<bool(HullCollider*)> lambda)
    {
        auto list = Every<HullCollider>();
        for(unsigned long i = 0; i != list.size(); ++i)
        {
            auto collider = list.at(i);
            if(lambda(collider)) if(place_meeting(x, y, collider))
            {
                return true;
            }
            
        }
        return false;
    }
    double HullCollider::contact_filter(std::function<bool(HullCollider*)> lambda)
    {
        // Move our position in our movement's direction until we hit a filtered-in object or we finish our movement's magnitude.
        return move_contact_filter(Of<Motion>(this)->hspeed, Of<Motion>(this)->vspeed, lambda);
    }
    double HullCollider::move_contact_filter(double hvec, double vvec, std::function<bool(HullCollider*)> lambda)
    {
        // Move our position by a movement vector until we hit a filtered-in object or we finish the movement's magnitude.
        
        const int MAX_I = 8; // subpixel precision (MAX_I = 8 means 1/8th of a pixel)
        int i = 8;
        
        auto * p = Of<Position>(this);
        double & x = p->x;
        double & y = p->y;
        
        double maxDistance = vector_length(hvec, vvec); // maximum total travel
        double sfac = maximum(absolute(hvec), absolute(vvec)); // Used to get pixel chunks from the movement vector
       
        double moveX = hvec/sfac*i/MAX_I,
               moveY = vvec/sfac*i/MAX_I,
               totalMoved = 0;
       
        while ( totalMoved < maxDistance and i > 0 )
        {
            moveX = hvec/sfac * i/MAX_I * minimum(1, maxDistance - totalMoved);
            moveY = vvec/sfac * i/MAX_I * minimum(1, maxDistance - totalMoved);
           
            if (!place_meeting_filter(x + moveX*i/MAX_I, y + moveY*i/MAX_I, lambda))
            {
                x += moveX * i/MAX_I;
                y += moveY * i/MAX_I;
                totalMoved += vector_length(moveX * i/MAX_I, moveY * i/MAX_I);
               
                if(i < MAX_I) // 
                    break;
            }
            else
                i -= 1;
        }
     
        return totalMoved;
    }
    
    double HullCollider::move_outside_filter(double hvec, double vvec, std::function<bool(HullCollider*)> lambda)
    {
        // Move our position by a movement vector until we stop hitting a filtered-in object or finish our movement's magnitude.
        
        const int MAX_I = 8; // subpixel precision (MAX_I = 8 means 1/8th of a pixel)
        int i = 8;
        
        auto * p = Of<Position>(this);
        double & x = p->x;
        double & y = p->y;
        
        double maxDistance = point_distance(0, 0, hvec, vvec); // maximum total travel
        double sfac = maximum(hvec, vvec); // Used to get pixel chunks from the movement vector
       
        double moveX = hvec/sfac*i/MAX_I,
               moveY = vvec/sfac*i/MAX_I,
               totalMoved = 0;
       
        while ( totalMoved < maxDistance and i > 0 )
        {
            moveX = hvec/sfac * i/MAX_I * minimum(1, maxDistance - totalMoved);
            moveY = vvec/sfac * i/MAX_I * minimum(1, maxDistance - totalMoved);
           
            if (place_meeting_filter(x + moveX*i/MAX_I, y + moveY*i/MAX_I, lambda))
            {
                x += moveX * i/MAX_I;
                y += moveY * i/MAX_I;
                totalMoved += point_distance(0, 0, moveX * i/MAX_I, moveY * i/MAX_I);
               
                if(i < MAX_I) // 
                    break;
            }
            else
                i -= 1;
        }
     
        return totalMoved;
    }
    
    
    struct Moveable : public Component
    {
        Moveable(i_entityid myEntity);
    };
    Moveable::Moveable(i_entityid myEntity) : Component(myEntity)
    {
        MakeDependency<Motion>();
        MakeDependency<Position>();
    }
    
    
    struct ControlledCharacter : public Component
    {
        ControlledCharacter(i_entityid myEntity);
        double groundcontrol = 0.1;
        double aircontrol = 0.05;
        double groundfriction = 0.05;
        double speedcapfactor = 30;
        bool init( double x, double y, double hspeed, double vspeed, double width, double height, double xoffset, double yoffset, double gravity );
        
    };
    ControlledCharacter::ControlledCharacter(i_entityid myEntity) : Component(myEntity)
    {
        MakeDependency<HullCollider>();
        MakeDependency<BoxDrawable>();
        MakeDependency<Gravitational>();
    }
    bool ControlledCharacter::init( double x, double y, double hspeed, double vspeed, double width, double height, double xoffset, double yoffset, double gravity )
    {
        
        Of<Gravitational>(entityID)->gravity = gravity;
        Of<Motion>(entityID)->hspeed = hspeed;
        Of<Motion>(entityID)->vspeed = vspeed;
        Of<Position>(entityID)->x = x;
        Of<Position>(entityID)->y = y;
        Of<Hull>(entityID)->width = width;
        Of<Hull>(entityID)->height = height;
        Of<Hull>(entityID)->xoffset = xoffset;
        Of<Hull>(entityID)->yoffset = yoffset;
        return true;
    }
    Gravitational::Gravitational(i_entityid myEntity) : Component(myEntity), gravity(0)
    {
        MakeDependency<Moveable>();
    }
    
    
    struct Floor : public Component
    {
        Floor(i_entityid myEntity);
        bool init( double x, double y, double xoffset, double yoffset, double width, double height );
    };
    Floor::Floor(i_entityid myEntity) : Component(myEntity)
    {
        MakeDependency<HullCollider>();
        MakeDependency<BoxDrawable>();
    }
    bool Floor::init( double x, double y, double xoffset, double yoffset, double width, double height )
    {
        Of<Position>(entityID)->x = x;
        Of<Position>(entityID)->y = y;
        Of<Hull>(entityID)->width = width;
        Of<Hull>(entityID)->height = height;
        Of<Hull>(entityID)->xoffset = xoffset;
        Of<Hull>(entityID)->yoffset = yoffset;
        return true;
    }
    
    
    BoxDrawable::BoxDrawable(i_entityid myEntity) : Component(myEntity)
    {
        MakeDependency<Position>();
        MakeDependency<Hull>();
    }
    SDL_Rect * BoxDrawable::getShape()
    {
        shape = {int(ceil(Of<Position>(entityID)->x+Of<Hull>(entityID)->xoffset)),
                 int(ceil(Of<Position>(entityID)->y+Of<Hull>(entityID)->yoffset)),
                 int(round(Of<Hull>(entityID)->width)),
                 int(round(Of<Hull>(entityID)->height))};
        return &shape;
    }
    
    
    struct TexturedDrawable : public Component
    {
        TexturedDrawable(i_entityid myEntity);
        SDL_Texture * sprite;
        double xoffset, yoffset;
        bool init(const char * sarg);
    };
    TexturedDrawable::TexturedDrawable(i_entityid myEntity) : Component(myEntity), sprite(NULL), xoffset(0), yoffset(0)
    {
        MakeDependency<Position>();
    }
    bool TexturedDrawable::init(const char * sarg)
    {
        sprite = loadTexture( sarg, Sys::Renderer );
        return sprite != nullptr;
    }
    
    // Special state
    std::vector<Component*> List;
    
    /* Shared/generic functions */
    
    // Return a vector of every known instance of a given kind of component
    template<typename CType>
    std::vector<CType*> Every()
    {
        std::vector<CType*> rrr;
        for(unsigned long i = 0; i != List.size(); ++i)
        {
            CType* typedItem = dynamic_cast<CType*>(List.at(i));
            if(typedItem)
                rrr.emplace_back((CType*)(List.at(i)));
            
        }
        return rrr;
    }
    
    // Adds a component to the system via pointer
    template<typename CType>
    CType* Add(CType* NewComponent)
    {
        List.emplace_back(NewComponent);
        return NewComponent;
    }
    
    
    // Performs a given one-argument function with each of a given component as that argument, in stored order
    template<typename CType>
    void With(std::function<void(CType*)> lambda)
    {
        for(unsigned long i = 0; i != List.size(); ++i)
        {
            CType* typedItem = dynamic_cast<CType*>(List.at(i));
            if(typedItem)
                lambda(typedItem);
            
        }
    }
    
    // Performs a given one-argument function a given component instance
    template<typename CType>
    void With(CType* component, std::function<void(CType*)> lambda)
    {
        lambda(component);
    }
    
    // Of -- returns a pointer to a specific component which belongs to a given entity, or else NULL
    template<typename CType>
    CType * Of (i_entityid myEntity)
    {
        unsigned long i;
        for(i = 0;
            i < List.size()
            && (dynamic_cast<CType*>(List.at(i)) == NULL
                || List.at(i)->entityID != myEntity);
            ++i);
        
        if(i == List.size())
            return NULL;
        else
            return (CType*)List.at(i);
        
    }

    // Of -- An "of" that finds via another component
    template<typename CType, typename CTypeFrom>
    CType * Of (CTypeFrom * Component)
    {
        return Of<CType>(Component->entityID);
    }

    // Refers -- Returns whether an entity owns a given type of component
    template<typename CType>
    bool Refers ( i_entityid myEntity )
    {
        unsigned long i;
        for(i = 0;
            i < List.size()
            && (dynamic_cast<CType*>(List.at(i)) == NULL
                || List.at(i)->entityID != myEntity);
            ++i);
        
        return i != List.size();
    }
    
    // First -- Returns the first of a given component
    template<typename CType>
    CType * First ()
    {
        auto e = Every<CType>();
        return e.front();
    }
    
    
    namespace Sys
    {
        std::vector<bool(*)()> tems; // Sys::tems
        
        bool FrameLimit()
        {
            // Frame limit
            double TimeWaitS;
            double TimeSpent;
            double prehalttime = Time::get_us();
            double halttime;
            
            if( Time::dostart )
            {
                SDL_Delay(1000.0/Time::Framerate);
                Time::dostart = false;
                Time::deviance = 0;
                halttime = Time::get_us();
            }
            else
            {
                prehalttime = Time::get_us();
                
                TimeSpent = prehalttime - Time::simstart_us;
                TimeWaitS = maximum(0.0, (1000.0 / Time::Framerate) - TimeSpent/1000.0 - Time::deviance);
                //TimeWaitS = maximum(0, Time::Frametime - TimeSpent/1000.0);
                
                SDL_Delay(round(TimeWaitS));
                
                halttime = Time::get_us();
                
                Time::error = (halttime - prehalttime)/1000.0 - round(TimeWaitS);
                Time::deviance = (halttime - prehalttime)/1000.0 - TimeWaitS;
                
                Time::ticks = fmod(Time::ticks + 1.0, Time::Framerate);
            }
            // Buffer timings
            Time::last_us = Time::simstart_us;
            Time::delta_us = Time::simstart_us - Time::last_us;
            Time::delta = Time::delta_us * Time::scale;
            Time::simstart_us = halttime;
            Time::frames.push_back( Time::simstart_us );

            // Throw away old timings
            while ( Time::frames.size() > int(Time::Framerate) )
                Time::frames.erase( Time::frames.begin() );
            
            if(true) // debug
            {
                std::cout << std::fixed << std::setprecision(2)
                          <<"\rfps "    << Time::scale / ((Time::frames.back() - Time::frames.front())/Time::frames.size())
                          << " sim "    << TimeSpent / 1000
                          //<< " bad "    << (Time::last - Time::simstart_us) * 1000
                          //<< " ask "    << std::setprecision(3) << TimeWaitS
                          << " halt "   << std::setprecision(3) << halttime-prehalttime
                          //<< " miss "   << std::setprecision(0) << Time::deviance * 1000
                          << " tick "   << Time::ticks
                          << " dev "  << Time::deviance << "\n";
            }
            
            return false;
        }
        
        bool RotateTimings()
        {
            return false;
        }
        
        struct inputContainer
        {
            struct mouseContainer
            {
                int x = 0;
                int y = 0;
                bool m1 = 0;
                bool m2 = 0;
                bool m3 = 0;
                bool m4 = 0;
                bool m5 = 0;
                int up = 0;
                int down = 0;
            } mouse;
            
            struct keysContainer
            {
                bool up = 0;
                bool down = 0;
                bool left = 0;
                bool right = 0;
            } keys;
            
        } input;
        struct inputContainer lastInput;
        
        int KeyStateLength = 0;
        const Uint8 *KeyState = SDL_GetKeyboardState(&KeyStateLength);
        Uint8 * KeyStatePrevious = (Uint8*)(malloc(sizeof(Uint8) * KeyStateLength));
        SDL_Event event;
        bool quit = false;
        
        bool PumpInput()
        {
            // TODO: Prevent the subframe sample loss bug
            // Test inputs (apparently)
            std::copy(KeyState, KeyState+KeyStateLength, KeyStatePrevious);
            
            lastInput = input;
            SDL_PumpEvents();
            while ( SDL_PollEvent( &event ) )
            {
                switch ( event.type )
                {
                    case SDL_QUIT:
                        quit = true;
                        break;
                    case SDL_MOUSEBUTTONDOWN:
                        if ( event.button.button == SDL_BUTTON_LEFT )
                        {
                            auto me = First<ControlledCharacter>();
                            Of<Position>(me)->x = event.button.x;
                            Of<Position>(me)->y = event.button.y;
                            Of<Motion>(me)->vspeed = 0;
                        }
                        else if ( event.button.button == SDL_BUTTON_RIGHT )
                        {
                            auto me = First<ControlledCharacter>();
                            Of<Motion>(me)->hspeed = ((Of<Position>(me)->x < event.button.x)*2-1)*10;
                        }
                        break;
                    case SDL_KEYDOWN:
                        input.keys.up |= event.key.keysym.sym == SDLK_e;
                        input.keys.down |= event.key.keysym.sym == SDLK_d;
                        input.keys.left |= event.key.keysym.sym == SDLK_w;
                        input.keys.right |= event.key.keysym.sym == SDLK_f;
                        break;
                    case SDL_KEYUP:
                        input.keys.up &= event.key.keysym.sym != SDLK_e;
                        input.keys.down &= event.key.keysym.sym != SDLK_d;
                        input.keys.left &= event.key.keysym.sym != SDLK_w;
                        input.keys.right &= event.key.keysym.sym != SDLK_f;
                        break;
                        
                }
            }
            SDL_GetMouseState(&input.mouse.x, &input.mouse.y);
            
            return false;
        }
        
        bool ControlPlayer()
        {
            With<ControlledCharacter>( [](ControlledCharacter * component)
            {
                std::function<bool(HullCollider*)> myFilter = [component](HullCollider * object)
                {
                    return object->entityID != component->entityID;
                };
                
                auto * m = Of<Motion>(component);
                auto * p = Of<Position>(component);
                auto * h = Of<HullCollider>(component);
                double & x = p->x;
                double & y = p->y;
                double & hspeed = m->hspeed;
                double & vspeed = m->vspeed;
                
                if(input.keys.up and !lastInput.keys.up)
                    vspeed -= 4;
                    
                if(!h->place_meeting_filter(x, y+1, myFilter))
                {
                    vspeed += Of<Gravitational>(component)->gravity;
                    if(input.keys.left)
                        hspeed -= component->aircontrol;
                    if(input.keys.right)
                        hspeed += component->aircontrol;
                    // TODO: Put into control case
                    hspeed = minimum(component->speedcapfactor*component->groundcontrol, absolute(hspeed)) * sign(hspeed);
                }
                else if (!h->place_meeting_filter(x, y, myFilter))
                {
                    vspeed = minimum(0, vspeed);
                    if(input.keys.left)
                        hspeed -= component->groundcontrol * ((hspeed > 0) + 1);
                    if(input.keys.right)
                        hspeed += component->groundcontrol * ((hspeed < 0) + 1);
                    // TODO: Put into control case
                    hspeed = minimum(component->speedcapfactor*component->groundcontrol, absolute(hspeed)) * sign(hspeed);
                    if(input.keys.right == input.keys.left)
                        hspeed = maximum(0, absolute(hspeed) - component->groundfriction) * sign(hspeed);
                }
                else
                {
                    hspeed = 0;
                    vspeed = 0;
                }
            } );
            
            return false;
        }
        
        bool MoveMovers()
        {   
            With<Moveable>( [](Moveable * component)
            {
                std::function<bool(HullCollider*)> myFilter = [component](HullCollider * object)
                {
                    return object->entityID != component->entityID;
                };
                
                auto * m = Of<Motion>(component);
                auto * p = Of<Position>(component);
                auto * h = Of<HullCollider>(component);
                double & x = p->x;
                double & y = p->y;
                double & hspeed = m->hspeed;
                double & vspeed = m->vspeed;
                
                if(h)
                {
                    if(!h->place_meeting_filter(x + hspeed, y + vspeed, myFilter))
                    {
                        x += hspeed;
                        y += vspeed;
                    }
                    else if (Of<ControlledCharacter>(component))
                    {
                        double hleft, vleft;
                        
                        hleft = hspeed;
                        vleft = vspeed;
                        
                        int loopCounter;
                        bool collisionRectified, stuck;
                        loopCounter = 0;
                        stuck = false;
                        
                        // slide in an appropriate direction to get outside of walls
                        if(h->place_meeting_filter(x, y, myFilter))
                            h->move_outside_filter(0, -32, myFilter);
                        if(h->place_meeting_filter(x, y, myFilter))
                        {
                            p->y += 32;
                            h->move_outside_filter(0, 32, myFilter);
                        }
                        if(h->place_meeting_filter(x, y, myFilter))
                            h->move_outside_filter(-32, 0, myFilter);
                        if(h->place_meeting_filter(x, y, myFilter))
                        {
                            p->x += 32;
                            h->move_outside_filter(32, 0, myFilter);
                        }
                        
                        std::cout << std::fixed << std::setprecision(3)
                                  << "\nx is " << x
                                  << ". y is " << y
                                  << ". h is " << hspeed
                                  << ". v is " << vspeed;
                        auto m = h->contact_filter(myFilter);
                        if (m > vector_length(hspeed, vspeed))
                        {
                            std::cout << "CONTACT_FILTER MADE AN ERROR AT" << x << y << hspeed << vspeed;
                            return;
                        }
                        
                        while((absolute(hleft) >= 1 || absolute(vleft) >= 1) && !stuck) // while we still have distance to travel
                        {
                            if(false)
                            {
                                std::cout << std::fixed << std::setprecision(3)
                                          << "\nLoop:" << loopCounter
                                          << "\nx is " << x
                                          << ". y is " << y
                                          << ". h is " << hspeed
                                          << ". v is " << vspeed;
                            }
                            loopCounter += 1;
                            if(loopCounter > 10) // After 10 loops, it's assumed we're stuck.
                                stuck = 1;
                            
                            collisionRectified = false; // set this to true when we fix a collision problem
                            // (eg. detect hitting the ceiling and setting vspeed = 0)
                            // if, after checking for all our possible collisions, we realize that we haven't
                            // been able to fix a collision problem, then we probably hit a corner or something,
                            // and we should try to fix that
                            
                            double prevX, prevY;
                            prevX = x;
                            prevY = y;
                            // move as far as we can without hitting something
                            h->move_contact_filter(hleft, vleft, myFilter);
                            // deduct that movement from our remaining movement
                            hleft -= x - prevX;
                            vleft -= y - prevY;
                            
                            // determine what we hit, and act accordingly
                            
                            if(vleft != 0 && h->place_meeting_filter(x, y + sign(vleft), myFilter)) // we hit a ceiling or floor
                            {
                                vleft = 0; // don't go up or down anymore
                                vspeed = 0; // don't try it next frame, either
                                collisionRectified = true;
                            }
                            
                            if(hleft != 0 && h->place_meeting_filter(x + sign(hleft), y, myFilter)) // we hit a wall on the left or right
                            {
                                if(!h->place_meeting_filter(x + sign(hleft), y - 6, myFilter)) // if we could just walk up the step
                                {
                                    y -= 6; // hop up the step.
                                    collisionRectified = true;
                                }
                                else if(!h->place_meeting_filter(x + sign(hleft), y + 6, myFilter) and absolute(hspeed) >= absolute(vspeed)) // ceiling sloping
                                {
                                    y += 6;
                                    collisionRectified = true;
                                }
                                else // it's not just a step, we've actually gotta stop
                                {
                                    hleft = 0; // don't go left or right anymore
                                    hspeed = 0; // don't try it next frame, either
                                    collisionRectified = true;
                                }
                            }
                            if(!collisionRectified && (absolute(hleft) >= 1 || absolute(vleft) >= 1)) // uh-oh, no collisions fixed, try stopping all vertical movement and see what happens
                            {
                                vspeed = 0;
                                vleft = 0;
                            }
                        }
                    }
                    else
                    {
                        return;
                        m->setPolar(0, 0);
                    }
                }
                else
                {
                    p->x += m->hspeed;
                    p->y += m->vspeed;
                }
            } );
            
            return false;
        }
        
        bool RenderThings()
        {
            if(false)
            {
                std::cout << std::fixed << std::setprecision(3)
                          << "\nx is " << Of<Position>(0)->x
                          << ". y is " << Of<Position>(0)->y
                          << ". h is " << Of<Motion>(0)->hspeed
                          << ". v is " << Of<Motion>(0)->vspeed;
            }

            // Clear screen
            // Cheap clear: use SDL_RenderClear() instead of SDL_RenderFillRect() if there are problems
            SDL_SetRenderDrawColor( Renderer, 0, 0, 0, 255);
            SDL_RenderFillRect( Renderer, &shape );
            
            // Draw Drawables
            With<TexturedDrawable>( [](TexturedDrawable * component)
            {
                renderTexture( component->sprite, Sys::Renderer, Of<Position>(component)->x, Of<Position>(component)->y );
            } );
            
            With<BoxDrawable>( [](BoxDrawable * component)
            {
                SDL_SetRenderDrawColor( Sys::Renderer, 255, 255, 255, 255 );
                SDL_RenderFillRect( Sys::Renderer, component->getShape() );
            } );
            return false;
        }
        
        bool PresentScreen()
        {
            SDL_RenderPresent(Renderer);
            return false;
        }
    }
}

namespace instance
{
    std::vector<i_entityid> Entities;
    i_entityid lowest_unused_id = 0;
    
    i_entityid allocate_id()
    {
        Entities.push_back(lowest_unused_id);
        std::sort(Entities.begin(), Entities.end());
        i_entityid i = 0;
        for(; std::find(Entities.begin(), Entities.end(), i) != Entities.end(); ++i);
        lowest_unused_id = i;
        return i;
    }
    
    template <typename CType>
    CType * create()
    {
        i_entityid id = allocate_id();
        
        return new CType(id);
    }
    
    int kill(i_entityid id)
    {
        unsigned long i;
        //bool worked;
        auto & List = World::List;
        for(i = 0;
            i < List.size();
            ++i)
        {
            if(List.at(i)->entityID == id)
            {
                delete List.at(i);
                List.erase(List.begin()+i);
                i -= 1;
                //worked = true;
            }
            
        }
        
        if(id < lowest_unused_id)
            lowest_unused_id = id;
        // Return codes:
        // 0 No error, entity and components existed
        // 1 Entity existed but no components
        // 2 ID had associated componenets but no registration
        // 3 ID has no registration nor components
        // TODO: Implement proper error coding
        return i != List.size();
    }
}

using namespace World;

int main(int argc, char *argv[])
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cout << "Could not initialize SDL: " << SDL_GetError() << std::endl;
        return 1;
    }

    int MAX_X = 800, MAX_Y = 600;

    Sys::MainWindow = SDL_CreateWindow("Benetnasch", 300, 300, MAX_X, MAX_Y, SDL_WINDOW_SHOWN);
    if (Sys::MainWindow == nullptr)
    {
        std::cout << "Could not create an SDL window: " << SDL_GetError() << std::endl;
        return 1;
    }
    Sys::Renderer = SDL_CreateRenderer(Sys::MainWindow, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC);
    if (Sys::Renderer == nullptr)
    {
        std::cout << "Could not create an SDL renderer: " << SDL_GetError() << std::endl;
        return 1;
    }

    srand(time(NULL));

    // Below: lots of ad-hoc to make sure shit is just working right
    
    std::copy(Sys::KeyState, Sys::KeyState+Sys::KeyStateLength, Sys::KeyStatePrevious);
    
    Sys::tems.push_back(&Sys::FrameLimit);
    Sys::tems.push_back(&Sys::RotateTimings);
    Sys::tems.push_back(&Sys::PumpInput);
    Sys::tems.push_back(&Sys::ControlPlayer);
    Sys::tems.push_back(&Sys::MoveMovers);
    Sys::tems.push_back(&Sys::RenderThings);
    Sys::tems.push_back(&Sys::PresentScreen);
    
    auto * t = instance::create<TexturedDrawable>();
    t->init("sprites/collision_map.png");
    
    auto * c = instance::create<ControlledCharacter>();
    c->init(600, 480, 0, 10, 7, 15, -3, -7,  0.075);
    
    // init( double x, double y, double xoffset, double yoffset, double width, double height, SDL_Renderer * Renderer )
    auto * f = instance::create<Floor>();
    f->init(0, 500, 0, 0, 800, 5);
    
    f = instance::create<Floor>();
    f->init(50, 450, 0, 0, 20, 55);
    
    SDL_PumpEvents();
    
    while ( !Sys::quit )
    {
        auto i = 0;
        for(auto function : Sys::tems)
        {
            auto r = function();
            if(r)
                Sys::tems.erase(Sys::tems.begin()+i);
            else
                i += 1;
        }
    }
    return 0;
}
