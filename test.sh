#!/bin/bash

source='benetnasch.cpp'
cflags='-g -ggdb -Wall -pedantic -mconsole '
linker='-lmingw32 -lSDL2main -lSDL2_image -lSDL2'

cmd="g++ $source $cflags $linker"

run='./a.exe'

echo
echo $cmd
echo

eval $cmd

if [ "$1" == "-r" ]; then
    eval $run
fi
